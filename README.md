# Izbori za zastupnike u Hrvatski sabor (2024-04-17)

## Sadržaj:

### Privremeni izborni rezultati

Podatkovni okvir `data/rezultati/data.rds` sadrži podatke učitane iz svih JSON datoteka s privremenim izbornim rezultatima i izvor je za izvedene podatkovne okvire. Preuzeto je sljedeće:

* `source_file`: naziv JSON datoteke.

* `time`: vrijeme (@datum, @vrijeme, u POSIXct formatu).

* `el_type`: vrsta izbora (@izboriOznaka).

* `ij`: izborna jedinica (@ijSifra).

* `lau_code`: oznaka geografske jedinice, u pravilu grada/općine (izvedeno iz naziva JSON datoteke).

* `bm`, `bm_total`, `bm_closed`, `bm_closed_prop`: oznaka biračkog mjesta (@bmSifra), ukupan broj biračkih mjesta (@bmUkupno), broj zatvorenih biračkih mjesta (@bmZatvoreno) i proporcija zatvorenih biračkih mjesta (izračunato).

* `note`, `note2`: bilješke (@napomena, @napomena2).

* `voters`, `voted`, `turnout`: broj upisanih birača (@biraciUkupno), broj birača koji su glasali (@biraciGlasovalo) i proporcija birača koji su glasali (izračunato).

* `ballots_total`, `ballots_valid`, `ballots_invalid`: ukupan broj glasačkih listića (@listiciUkupno), broj važećih listića (@listiciVazeci), broj nevažećih listića (@listiciNevazeci).

* `el_lists`: podaci o kandidacijskim listama i glasovima:
    * `list_order`: poredak na listi (@rbr).
	* `list_code`, `list_name`: šifra liste (@jedinstvenaSifra) i naziv liste na nacionalnoj razini (određeno prilagodbom DIP-ovog šifrarnika).
	* `votes`, `votes_perc`, `votes_prop`: broj glasova (@glasova), postotak glasova (@posto) i proporcija glasova (izračunato).

Izvedeni podatkovni okvir `data/rezultati/data_jls.rds` sadrži podatke za izbor zastupnika u 1. do 11. izbornoj jedinici i uključuje glasove s posebnih biračkih mjesta i biračkih mjesta u inozemstvu.

Izvedeni podatkovni okvir `data/rezultati/data_jls_map.rds` je u simple feature collection formatu (`sf`) na razini geografskih jedinica (u pravilu gradova/općina) i sadrži, uz geometriju geografskih jedinica (`geometry)`, podatke o izboru zastupnika u 1. do 10. izbornoj jedinici, ali **ne uključuje** glasove s posebnih biračkih mjesta i biračkih mjesta u inozemstvu (jer DIP ne pruža informaciju u kojoj se geografskoj jedinici ti glasovi pribrajaju).

Izvedeni podatkovni okvir `data/rezultati/data_ij.rds` sadrži podatke o izboru zastupnika u 1. do 11. izbornoj jedinici na razini izbornih jedinica i uključuje meta-podatke (`ij`, `bm_total`, `bm_closed`, `voters`, `voted`, `ballots_total`, `ballots_valid`, `ballots_invalid`), podatkovni okvir `el_seats` s podacima o broju glasova i osvojenih mandata za kandidacijske liste (`ij`, `list_code`, `list_name`, `votes`, `votes_prop`, `seats`).

Izvedene datoteke su dostupne u alternativnim formatima (u nešto smanjenom obimu zbog složenosti originalne strukture):

* CSV: `data/rezultati/data_jls.csv`, `data/rezultati/data_ij.csv`

* Geoformati: `data/rezultati/data_jls_map.geojson`, `data/rezultati/data_jls_map.shp` 

Podaci o ukupnom broju obrađenih biračkih mjesta ne mogu se izvesti iz prethodnih podataka, ali dostupni su u sumarnoj JSON datoteci i mogu se dobiti posebnom funkcijom `get_bm_global()`.

### Podaci o odazivu

Podaci o odazivu birača objavljeni na izborni dan u 11:30 i 16:30 preuzeti su iz posebnih JSON datoteka koje se nalaze u direktoriju `data/json_files_odaziv`. Podaci o odazivu na razini izbornih jedinica dostupni su u podatkovnom okviru `data/rezultati/turnout_ij.rds`, a na razini gradova/općina u podatkovnom okviru `data/rezultati/turnout_jls.rds`.

Dostupni su sljedeći podaci:

* `ij`, `lau_code`, `lau_name_dip`: šifra izborne jedinice ili grada/općine (@sifra), naziv grada/općine (@naziv).

* `timepoint`: vrijeme do kojeg je zabilježen odaziv (11:30, 16:30).

* `voters`: ukupan broj birača (@ukBir).

* `bm_total`, `bm_reported`: ukupan broj biračkih mjesta (@ukBm) i broj biračkih mjesta koja su dojavila podatak o odazivu (@bmDojavila).

* `bm_reported_voters`, `bm_reported_voted`: ukupan broj birača na biračkim mjestima koja su dojavila podatak o odazivu (@bmDojavilaBrBir), broj birača koji su na njima glasali (@ukGlas).

* `turnout`: odaziv, proporcija birača koji su glasali (izračunato).


### Ostalo

* `data/dip_upute`: Upute Državnog izbornog povjerenstva medijima za preuzimanje podataka, uključujući šifrarnike, opis sloga i ogledne JSON dokumente. 

* `data/izborne_liste.rds`: Šifrarnik lista objedinjenih na nacionalnoj razini (IJ 1-11).

* `data/karta`: Karta izbornih jedinica na LAU2 razini u `sf` i `.shp` formatu, usklađena s kodovima iz šifrarnika Državnog izbornog povjerenstva; Zagreb je podijeljen na tri izborne cjeline (01331, 01332, 01336).

* `fun.R`: Funkcije za učitavanje podataka iz JSON datoteka, D'Hondt izračun, itd.

* `ankete_ij.R`: Predizborne ankete po izbornim jedinicama: HRejting (HRT), Ipsos (Nova TV).

## Napomene

* Podatke o preferencijalnim glasovima se (zasad) ne obrađuje.

* Podatke o manjinskim izborima se (zasad) ne obrađuje, ali su dostupni u osnovnom podatkovnom okviru.

* Ovo je neslužbeni izvor podataka, nema ikakvih jamstava. *Use at your own peril!*
